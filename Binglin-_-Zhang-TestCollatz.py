#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, collatz_eval_helper, cache_eval

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "13 12\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  13)
        self.assertEqual(j, 12)

    def test_read_3(self):
        s = "8 8\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  8)
        self.assertEqual(j, 8)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(1000, 3444)
        self.assertEqual(v, 217)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)

    def test_eval_6(self):
        v = collatz_eval(130, 130)
        self.assertEqual(v, 29)

    def test_eval_7(self):
        v = collatz_eval(1001, 3444)
        self.assertEqual(v, 217)

    def test_eval_8(self):
        v = collatz_eval(4999, 87001)
        self.assertEqual(v, 351)

    # ----
    # cache eval
    # ----

    def test_cache_eval_1(self):
        v = cache_eval(48, 7356)
        self.assertEqual(v, 262)

    def test_cache_eval_2(self):
        v = cache_eval(266, 26509)
        self.assertEqual(v, 282)

    def test_cache_eval_3(self):
        v = cache_eval(9039, 16404)
        self.assertEqual(v, 276)

    def test_cache_eval_4(self):
        v = cache_eval(9039, 99999)
        self.assertEqual(v, 351)

    def test_cache_eval_5(self):
        v = cache_eval(19039, 999999)
        self.assertEqual(v, 525)

    def test_cache_eval_6(self):
        v = cache_eval(92093, 233333)
        self.assertEqual(v, 443)

    # ----
    # eval helper
    # ----

    def test_helper_1(self):
        v = collatz_eval_helper(3)
        self.assertEqual(v, 8)

    def test_helper_2(self):
        v = collatz_eval_helper(130)
        self.assertEqual(v, 29)

    def test_helper_3(self):
        v = collatz_eval_helper(2333)
        self.assertEqual(v, 33)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 1, 1, 1)
        self.assertEqual(w.getvalue(), "1 1 1\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 10, 1, 20)
        self.assertEqual(w.getvalue(), "10 1 20\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("1 10\n10 1\n130 130\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n10 1 20\n130 130 29\n")

    def test_solve_3(self):
        r = StringIO("1      1000     \n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1000 179\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
